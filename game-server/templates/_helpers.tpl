{{- define "game-instance.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "game-instance.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}

{{- define "game-instance.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "game-instance.labels" -}}
helm.sh/chart: {{ include "game-instance.chart" . }}
{{ include "game-instance.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "game-instance.selectorLabels" -}}
app.kubernetes.io/name: {{ include "game-instance.name" . }}
app.kubernetes.io/instance: {{ include "game-instance.fullname" . }}
{{- end }}

{{- define "game-instance.persistentvolumeclaim" -}}
{{- printf "%s-data" (include "game-instance.fullname" .) }}
{{- end }}

