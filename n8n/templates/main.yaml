apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ printf "%s-main" .Release.Name | quote }}
  labels:
    app.kubernetes.io/name: {{ .Chart.Name | quote }}
    app.kubernetes.io/instance: {{ printf "%s-%s" .Release.Name .Chart.Name | quote }}
    app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
    app.kubernetes.io/component: "main"
spec:
  {{- if .Values.persistence.enabled }}
  replicas: 1
  {{- else }}
  replicas: {{ .Values.main.replica.count }}
  {{- end }}
  {{- if .Values.persistence.enabled }}
  strategy:
    type: Recreate
  {{- end }}
  selector:
    matchLabels:
      app.kubernetes.io/app: {{ printf "%s-main" .Release.Name | quote }}
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{ .Chart.Name | quote }}
        app.kubernetes.io/app: {{ printf "%s-main" .Release.Name | quote }}
        app.kubernetes.io/instance: {{ printf "%s-%s" .Release.Name .Chart.Name | quote }}
        app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
        app.kubernetes.io/component: "main"
    spec:
      securityContext:
        fsGroup: 1000
        fsGroupChangePolicy: OnRootMismatch
      containers:
        - name: main
          image: "{{ .Values.n8nImage }}:{{ .Values.n8nVersion | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.main.pullPolicy | quote }}
          envFrom:
            - secretRef:
                name: {{ printf "%s-env" .Release.Name | quote }}
          args:
            - "start"
          ports:
            - containerPort: {{ .Values.main.ports.http }}
              name: http
              protocol: TCP
          readinessProbe:
            httpGet:
              path: /healthz
              port: http
          {{- if .Values.persistence.enabled }}
          volumeMounts:
            - mountPath: /home/node/.n8n
              name: data
          {{- end }}
      {{- if .Values.persistence.enabled }}
      volumes:
        - name: data
          persistentVolumeClaim:
            claimName: {{ printf "%s-binary" .Release.Name | quote }}
      {{- end }}
---
{{- if .Values.persistence.enabled }}
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: {{ printf "%s-binary" .Release.Name | quote }}
  labels:
    app.kubernetes.io/name: {{ .Chart.Name | quote }}
    app.kubernetes.io/instance: {{ printf "%s-%s" .Release.Name .Chart.Name | quote }}
    app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
spec:
  accessModes:
    - ReadWriteOnce
  {{- if .Values.persistence.storageClass }}
  storageClassName: {{ .Values.persistence.storageClass | quote }}
  {{- end }}
  resources:
    requests:
      storage: {{ .Values.persistence.size | quote }}
---
{{- end }}
apiVersion: v1
kind: Service
metadata:
  name: {{ printf "%s-main" .Release.Name | quote }}
  labels:
    app.kubernetes.io/name: {{ .Chart.Name | quote }}
    app.kubernetes.io/instance: {{ printf "%s-%s" .Release.Name .Chart.Name | quote }}
    app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
    app.kubernetes.io/component: "main"
spec:
  type: ClusterIP
  selector:
    app.kubernetes.io/app: {{ printf "%s-main" .Release.Name | quote }}
  ports:
    - port: {{ .Values.main.ports.http }}
      targetPort: http
      protocol: TCP
      name: http
---
{{- if .Values.ingress.enabled }}
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: {{ printf "%s-main" .Release.Name | quote }}
  labels:
    app.kubernetes.io/name: {{ .Chart.Name | quote }}
    app.kubernetes.io/instance: {{ printf "%s-%s" .Release.Name .Chart.Name | quote }}
    app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
    app.kubernetes.io/component: "main"
  annotations:
    {{ toYaml .Values.ingress.annotations | indent 4 }}
spec:
  rules:
    - host: {{ .Values.ingress.host | quote }}
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: {{ printf "%s-main" .Release.Name | quote }}
                port:
                  name: http
{{- end }}
